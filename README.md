# mystatus

A fast status line generator for [`i3bar`][i3bar] written in [Rust][rust].

 [i3bar]: https://i3wm.org/docs/i3bar-protocol.html
 [rust]: https://www.rust-lang.org/
