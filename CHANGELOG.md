## Version 0.9.2 (01 Nov 2023)

 - Add `Cargo.toml` to version control
 - Use `cfg` macro to get program version

## Version 0.9.1 (07 Nov 2021)

 - Update version in command line

## Version 0.9 (17 Sep 2020)

 - Add actions
 - Shutdown on critical battery

## Version 0.8 (10 Oct 2019)

 - Use home partition for disk item

## Version 0.7 (10 Oct 2019)

 - Fix wraparound of value 100

## Version 0.6 (08 Sep 2018)

 - Battery module improvements

## Version 0.5 (26 Jan 2017)

 - Fix continuous refresh of network interface

## Version 0.4 (23 Jan 2017)

- Cosmetic changes
- Reset network data after the interface changes

## Version 0.3 (05 Jan 2017)

- Fix crash on 100% CPU
- Increase size of network fields

## Version 0.2 (02 Jan 2017)

- Add email and battery notifications
- Fix network interface detection

## Version 0.1 (31 Dec 2016)

- Initial release
