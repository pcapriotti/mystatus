use std::fs::File;
use std::io;
use std::io::BufRead;
use std::str::FromStr;

use display_item::{Action, DisplayItem, Icons, Item};

pub struct Cpu {
    info: Option<CpuInfo>,
    old_info: Option<CpuInfo>,
}

#[derive(Clone, Copy, Debug)]
pub struct CpuInfo {
    time: u64,
    total: u64,
}

impl DisplayItem for Cpu {
    fn display(&self, _icons: &dyn Icons) -> Item {
        let value = self.percent();
        let colour = Item::percent_colour(100 - value);
        let fmt = format!("Cpu {}", colour.apply_to(Item::percent(value)));
        Item::from_markup(fmt.as_str())
    }

    fn refresh(&mut self, _i: u32) -> Vec<Box<dyn Action>> {
        self.old_info = self.info;
        self.info = Cpu::get_info();
        Vec::new()
    }
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            info: None,
            old_info: None,
        }
    }

    fn percent(&self) -> u8 {
        let info = match self.info {
            None => return 0,
            Some(x) => x,
        };
        let old_info = match self.old_info {
            None => return 0,
            Some(x) => x,
        };

        let time = (info.time as i64 - old_info.time as i64) as u64;
        let total = (info.total as i64 - old_info.total as i64) as u64;
        (time * 100 / total) as u8
    }

    fn get_info() -> Option<CpuInfo> {
        fn get_info_result() -> io::Result<Option<CpuInfo>> {
            let f = File::open("/proc/stat")?;
            let buf = io::BufReader::new(&f);
            for mline in buf.lines() {
                let line = mline?;
                let mut splits = line.split_whitespace();
                if let Some("cpu") = splits.next() {
                    let values: Vec<u64> = splits.map(|x| u64::from_str(x).unwrap_or(0)).collect();
                    let result = CpuInfo {
                        total: values[0] + values[2] + values[3],
                        time: values[0] + values[2],
                    };
                    return Ok(Some(result));
                }
            }
            Ok(None)
        }
        get_info_result().unwrap_or(None)
    }
}
