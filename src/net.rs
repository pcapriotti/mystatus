use std::convert::From;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::num::ParseIntError;
use std::ops::Add;
use std::process::Command;
use std::str;
use std::str::FromStr;

use display_item::{Action, Colour, DisplayItem, Icons, Item};

pub struct Net {
    iface: Option<String>,
    info: Option<NetInfo>,
    old_info: Option<NetInfo>,
}

#[derive(Clone, Copy, Debug)]
struct NetInfo {
    upload: u64,
    download: u64,
}

struct NetInfoError {
    _description: String,
}

impl DisplayItem for Net {
    fn display(&self, icons: &dyn Icons) -> Item {
        match self.get_display() {
            None => Item::empty(),
            Some((upload, download)) => {
                let mut json1 = Item::markup(
                    format!(
                        "{} {}/s",
                        Colour::Cyan.apply_to(icons.upload()),
                        Item::size(upload)
                    )
                    .as_str(),
                );
                json1["min_width"] = "HHHHHHHHHHHHH".into();
                let item1 = Item::from_json(json1);
                let mut json2 = Item::markup(
                    format!(
                        "{} {}/s",
                        Colour::Magenta.apply_to(icons.download()),
                        Item::size(download)
                    )
                    .as_str(),
                );
                json2["min_width"] = "HHHHHHHHHHHHH".into();
                let item2 = Item::from_json(json2);

                item1 + item2
            }
        }
    }

    fn refresh(&mut self, i: u32) -> Vec<Box<dyn Action>> {
        if let Some(ref iface) = self.iface {
            self.old_info = self.info;
            self.info = Some(NetInfo::new(iface));
        }

        // update network device
        if i % 10 == 0 {
            let iface = Self::get_iface().ok();
            if iface != self.iface {
                self.iface = Self::get_iface().ok();
                self.info = None;
                self.old_info = None;
            }
        }

        Vec::new()
    }
}

impl Net {
    pub fn new() -> Net {
        if let Ok(iface) = Self::get_iface() {
            let info = NetInfo::new(&iface);
            Net {
                iface: Some(iface), // todo
                info: Some(info),
                old_info: None,
            }
        } else {
            Net {
                iface: None,
                info: None,
                old_info: None,
            }
        }
    }

    fn get_display(&self) -> Option<(u64, u64)> {
        let info = match self.info {
            None => return None,
            Some(i) => i,
        };
        let old_info = match self.old_info {
            None => return None,
            Some(i) => i,
        };
        let upload = info.upload as i64 - old_info.upload as i64;
        let download = info.download as i64 - old_info.download as i64;
        Some((upload as u64, download as u64))
    }

    fn get_iface() -> Result<String, String> {
        let cmd = Command::new("ip")
            .arg("route")
            .arg("get")
            .arg("8.8.8.8")
            .output()
            .map_err(|e| e.to_string())?;
        let out = str::from_utf8(&cmd.stdout).unwrap_or("");
        let line = out.split('\n').nth(0).unwrap_or("");
        let values: Vec<_> = line.split_whitespace().collect();
        if values.len() > 4 {
            Ok(values[4].to_string())
        } else {
            Err("invalid net info line".to_string())
        }
    }
}

impl NetInfo {
    fn new(iface: &str) -> NetInfo {
        NetInfo::new_result(iface).unwrap_or(NetInfo::default())
    }

    fn new_result(iface: &str) -> Result<NetInfo, NetInfoError> {
        let title = iface.to_owned().add(":");
        let f = File::open("/proc/net/dev")?;
        let buf = io::BufReader::new(&f);
        for mline in buf.lines() {
            let line = mline?;
            let values: Vec<_> = line.split_whitespace().collect();
            if values.len() >= 9 && values[0] == title {
                let download = u64::from_str(values[1])?;
                let upload = u64::from_str(values[9])?;
                return Ok(NetInfo { upload, download });
            }
        }
        Err(NetInfoError::new(
            "Could not find network interface".to_owned(),
        ))
    }

    fn default() -> NetInfo {
        NetInfo {
            upload: 0,
            download: 0,
        }
    }
}

impl From<io::Error> for NetInfoError {
    fn from(e: io::Error) -> NetInfoError {
        NetInfoError {
            _description: e.to_string(),
        }
    }
}

impl From<ParseIntError> for NetInfoError {
    fn from(e: ParseIntError) -> NetInfoError {
        NetInfoError {
            _description: e.to_string(),
        }
    }
}

impl NetInfoError {
    fn new(description: String) -> NetInfoError {
        NetInfoError {
            _description: description,
        }
    }
}
