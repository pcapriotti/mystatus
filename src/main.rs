extern crate chrono;
extern crate clap;
extern crate dbus;
#[macro_use(object)]
extern crate json;
mod battery;
mod clock;
mod cpu;
mod disk;
mod display_item;
mod mail;
mod net;
mod ram;

use chrono::Timelike;
use clap::{App, Arg};
use display_item::{DisplayItem, Icons};
use std::str;
use std::thread::sleep;
use std::time::Duration;

fn create_items(inbox: Option<&str>) -> Vec<Box<dyn DisplayItem>> {
    let mut items: Vec<Box<dyn DisplayItem + 'static>> = Vec::new();
    if let Some(b) = battery::Battery::new() {
        items.push(Box::new(b))
    }
    items.push(Box::new(ram::Ram {}));
    items.push(Box::new(cpu::Cpu::new()));
    items.push(Box::new(disk::Disk::new()));
    if let Some(path) = inbox {
        items.push(Box::new(mail::Mail::new(path.to_string())));
    }
    items.push(Box::new(net::Net::new()));
    items.push(Box::new(clock::Clock {}));

    items
}

fn main() {
    let matches = App::new("mystatus")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Paolo Capriotti")
        .about("Status line generator for i3bar")
        .arg(
            Arg::with_name("inbox")
                .short("i")
                .long("inbox")
                .value_name("DIR")
                .help("Inbox path for email notification"),
        )
        .get_matches();

    let inbox = matches.value_of("inbox");
    let icons = display_item::AwesomeIcons {};
    let items = create_items(inbox);

    println!("{}\n[", json::stringify(object!("version" => 1)));
    main_loop(&icons, items);
}

fn main_loop(icons: &dyn Icons, mut items: Vec<Box<dyn DisplayItem>>) {
    let mut i = 0;
    loop {
        sleep_to_next_second();
        for item in &mut items {
            for act in item.refresh(i) {
                act.execute().unwrap_or(());
            }
        }
        i += 1;
        println!("{},", json::stringify(display_all(icons, &items)));
    }
}

fn sleep_to_next_second() {
    let now = chrono::Local::now();
    let nanos = now.nanosecond() % 1_000_000_000;
    let nsec = 1_000_000_000 - nanos;
    sleep(Duration::new(0, nsec));
}

fn display_all(icons: &dyn Icons, items: &Vec<Box<dyn DisplayItem>>) -> json::JsonValue {
    let mut results = json::JsonValue::new_array();
    for item in items {
        let values = item.display(icons).values;
        for value in values {
            results.push(value).unwrap_or(());
        }
    }
    results
}
