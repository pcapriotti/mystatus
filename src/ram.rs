use std::fs::File;
use std::io;
use std::io::BufRead;
use std::str::FromStr;

use display_item::{DisplayItem, Icons, Item};

pub struct Ram {}

struct RamInfo {
    available: u64,
    total: u64,
}

impl RamInfo {
    pub fn percent(&self) -> u8 {
        (self.available * 100 / self.total) as u8
    }
}

impl DisplayItem for Ram {
    fn display(&self, _icons: &dyn Icons) -> Item {
        self.get_display().unwrap_or(Item::empty())
    }
}

impl Ram {
    fn get_ram_info(&self) -> io::Result<RamInfo> {
        let f = File::open("/proc/meminfo")?;
        let buf = io::BufReader::new(&f);
        let mut result = RamInfo {
            available: 0,
            total: 1,
        };
        for mline in buf.lines() {
            let line = mline?;
            let mut splits = line.split_whitespace();
            match splits.next() {
                Some("MemAvailable:") => {
                    let s = splits.next().unwrap_or("");
                    let value = u64::from_str(s).unwrap_or(0);
                    result.available = value * 1024;
                }
                Some("MemTotal:") => {
                    let s = splits.next().unwrap_or("");
                    let value = u64::from_str(s).unwrap_or(1);
                    result.total = value * 1024;
                }
                _ => continue,
            }
        }
        Ok(result)
    }

    fn get_display(&self) -> io::Result<Item> {
        let info = self.get_ram_info()?;
        let fmt = format!(
            "Ram {} {}",
            Item::size(info.available),
            Item::coloured_percent(info.percent())
        );
        Ok(Item::from_markup(fmt.as_str()))
    }
}
