use dbus;
use json;
use std::ops::Add;
use std::process::Command;

const PERCENT_VALUES: [char; 8] = ['▁', '▂', '▃', '▄', '▅', '▆', '▇', '█'];

pub struct Item {
    pub values: Vec<json::JsonValue>,
}

#[derive(Debug)]
pub struct Notification {
    summary: String,
    body: String,
    icon_name: Option<String>,
    sound_name: Option<String>,
}

#[derive(Debug)]
pub struct CommandAction {
    pub command: String,
    pub args: Vec<String>,
}

pub trait Action {
    fn execute(&self) -> Result<(), String>;
}

pub trait DisplayItem {
    fn refresh(&mut self, _i: u32) -> Vec<Box<dyn Action>> {
        Vec::new()
    }
    fn display(&self, _: &dyn Icons) -> Item;
}

impl Item {
    pub fn empty() -> Item {
        Item { values: Vec::new() }
    }

    pub fn from_json(x: json::JsonValue) -> Item {
        Item { values: vec![x] }
    }

    pub fn from_markup(s: &str) -> Item {
        Item::from_json(Item::markup(s))
    }

    pub fn markup(s: &str) -> json::JsonValue {
        object!("full_text" => s,
                "markup" => "pango")
    }

    pub fn size(n: u64) -> String {
        if n < 1024 {
            format!("{} B", n)
        } else if n < (1024 * 1024) {
            format!("{:.2} KiB", n as f32 / 1024.0)
        } else if n < 1024 * 1024 * 1024 {
            format!("{:.2} MiB", n as f32 / (1024.0 * 1024.0))
        } else {
            format!("{:.2} GiB", n as f32 / (1024.0 * 1024.0 * 1024.0))
        }
    }

    pub fn percent(x: u8) -> String {
        let index = (x as usize * 8) / 100;
        PERCENT_VALUES[std::cmp::min(index, 7)].to_string()
    }

    pub fn coloured_percent(x: u8) -> String {
        let s = Self::percent(x);
        Self::percent_colour(x).apply_to(s)
    }

    pub fn percent_level(x: u8) -> u8 {
        let value = x as u32;
        if value < 5 {
            0
        } else {
            (value / 25 + 1) as u8
        }
    }

    pub fn percent_colour(value: u8) -> Colour {
        Self::level_colour(Self::percent_level(value))
    }

    pub fn level_colour(level: u8) -> Colour {
        match level {
            0 | 1 => Colour::Red,
            2 => Colour::Yellow,
            3 => Colour::None,
            _ => Colour::Green,
        }
    }
}

impl Add for Item {
    type Output = Item;
    fn add(self, item: Item) -> Item {
        let mut values = self.values;
        let mut values1 = item.values;
        values.append(&mut values1);
        Item { values }
    }
}

#[derive(PartialEq, Eq)]
pub enum Colour {
    None,
    Green,
    Red,
    Yellow,
    Cyan,
    Magenta,
    Teal,
}

impl Colour {
    pub fn to_str(&self) -> &'static str {
        match *self {
            Colour::None => "#000000",
            Colour::Green => "#33aa33",
            Colour::Red => "#aa3333",
            Colour::Yellow => "#aaaa33",
            Colour::Cyan => "#33aaaa",
            Colour::Magenta => "#aa33aa",
            Colour::Teal => "#367588",
        }
    }

    pub fn apply_to(&self, s: String) -> String {
        if *self == Colour::None {
            s
        } else {
            format!("<span color=\"{}\">{}</span>", self.to_str(), s)
        }
    }
}

pub trait Icons {
    fn plug(&self) -> String;
    fn battery(&self, level: u8) -> String;
    fn calendar(&self) -> String;
    fn clock(&self) -> String;
    fn disk(&self) -> String;
    fn upload(&self) -> String;
    fn download(&self) -> String;
    fn mail(&self) -> String;
}

pub struct AwesomeIcons {}

impl Icons for AwesomeIcons {
    fn plug(&self) -> String {
        "\u{f1e6}".to_owned()
    }
    fn battery(&self, level: u8) -> String {
        match level {
            0 => "\u{f244}",
            1 => "\u{f243}",
            2 => "\u{f242}",
            3 => "\u{f241}",
            _ => "\u{f240}",
        }
        .to_owned()
    }
    fn calendar(&self) -> String {
        "\u{f073}".to_owned()
    }
    fn clock(&self) -> String {
        "\u{f017}".to_owned()
    }
    fn disk(&self) -> String {
        "\u{f1c0}".to_owned()
    }
    fn upload(&self) -> String {
        "\u{f093}".to_owned()
    }
    fn download(&self) -> String {
        "\u{f019}".to_owned()
    }
    fn mail(&self) -> String {
        "\u{f0e0}".to_owned()
    }
}

impl Notification {
    pub fn new(
        summary: String,
        body: String,
        icon_name: Option<String>,
        sound_name: Option<String>,
    ) -> Notification {
        Notification {
            summary,
            body,
            icon_name,
            sound_name,
        }
    }
}

impl Action for Notification {
    fn execute(&self) -> Result<(), String> {
        let c = dbus::Connection::get_private(dbus::BusType::Session)
            .map_err(|_| "Could not connect to session bus")?;
        let mut hints = Vec::new();
        if let Some(ref sound_name) = self.sound_name {
            let entry = dbus::MessageItem::DictEntry(
                Box::new(dbus::MessageItem::Str("sound-name".to_string())),
                Box::new(dbus::MessageItem::Variant(Box::new(
                    dbus::MessageItem::Str(sound_name.clone()),
                ))),
            );
            hints.push(entry);
        }
        let m = dbus::Message::new_method_call(
            "org.freedesktop.Notifications",
            "/org/freedesktop/Notifications",
            "org.freedesktop.Notifications",
            "Notify",
        )
        .map_err(|_| "Could not create method call")?;
        let m1 = m
            .append(dbus::MessageItem::Str("mystatus".to_owned()))
            .append(dbus::MessageItem::UInt32(0))
            .append(dbus::MessageItem::Str(
                self.icon_name.clone().unwrap_or("".to_owned()),
            ))
            .append(dbus::MessageItem::Str(self.summary.clone()))
            .append(dbus::MessageItem::Str(self.body.clone()))
            .append(dbus::MessageItem::Array(Vec::new(), "s".into()))
            .append(dbus::MessageItem::Array(hints, "{sv}".into()))
            .append(dbus::MessageItem::Int32(5000));
        c.send_with_reply_and_block(m1, 2000)
            .map_err(|e| match e.message() {
                None => "Could not create method call".to_string(),
                Some(x) => x.to_string(),
            })?;
        Ok(())
    }
}

impl Action for CommandAction {
    fn execute(&self) -> Result<(), String> {
        Command::new(&self.command)
            .args(&self.args)
            .output()
            .map_err(|e| e.to_string())?;
        Ok(())
    }
}
