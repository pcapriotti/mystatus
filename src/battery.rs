extern crate json;
use std::fs;
use std::fs::File;
use std::io;
use std::io::Read;
use std::path::{Path, PathBuf};
use std::str::FromStr;

use display_item::{Action, CommandAction, DisplayItem, Icons, Item, Notification};

pub struct Battery {
    path: PathBuf,
    total: u64,
    prefix: &'static str,
    info: Option<BatteryInfo>,
    old_info: Option<BatteryInfo>,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Status {
    Charging,
    Discharging,
}

#[derive(Clone, Copy)]
struct BatteryInfo {
    status: Status,
    charge: u64,
}

impl DisplayItem for Battery {
    fn display(&self, icons: &dyn Icons) -> Item {
        if let Some(info) = self.info {
            let value = self.value(&info);
            let markup = format!("{} {}%", Battery::symbol(icons, info.status, value), value);
            Item::from_markup(markup.as_str())
        } else {
            Item::empty()
        }
    }

    fn refresh(&mut self, _i: u32) -> Vec<Box<dyn Action>> {
        self.old_info = self.info;
        self.info = self.get_info();

        if let Some(info) = self.info {
            let value = self.value(&info);
            let old_value = if let Some(old_info) = self.old_info {
                self.value(&old_info)
            } else {
                100
            };
            if value >= 100 && old_value < 100 {
                return vec![Box::new(Notification::new(
                    "Battery full".to_string(),
                    String::new(),
                    Some("battery-full-charged".to_string()),
                    None,
                ))];
            }

            if value <= 1 && old_value > 1 {
                return vec![
                    Box::new(Notification::new(
                        "Battery critical".to_string(),
                        format!("{}% battery power left", value),
                        Some("battery-empty".to_string()),
                        Some("dialog-warning".to_string()),
                    )),
                    Box::new(CommandAction {
                        command: "sysctl".to_string(),
                        args: vec!["poweroff".to_string()],
                    }),
                ];
            }
        }
        Vec::new()
    }
}

impl Battery {
    pub fn new() -> Option<Battery> {
        Self::new_().ok()
    }

    fn new_() -> Result<Battery, String> {
        let entries = fs::read_dir("/sys/class/power_supply").map_err(|e| e.to_string())?;
        for mentry in entries {
            let entry = mentry.map_err(|e| e.to_string())?;
            for prefix in ["charge", "energy"].iter() {
                match Battery::get_total_charge(prefix, &entry.path()) {
                    Err(_) => continue,
                    Ok(total) => {
                        if let Ok(info) = Self::get_info_(prefix, &entry.path()) {
                            return Ok(Battery {
                                path: entry.path(),
                                total,
                                prefix,
                                old_info: None,
                                info: Some(info),
                            });
                        } else {
                            continue;
                        }
                    }
                }
            }
        }
        Err("Could not find battery".to_owned())
    }

    fn value(&self, info: &BatteryInfo) -> u8 {
        (info.charge * 100 / self.total) as u8
    }

    fn get_total_charge(prefix: &str, path: &Path) -> io::Result<u64> {
        let mut s = String::new();
        File::open(path.join(String::from(prefix) + "_full"))?.read_to_string(&mut s)?;
        Ok(u64::from_str(s.trim()).unwrap_or(1))
    }

    fn get_status(path: &Path) -> io::Result<Status> {
        let mut s = String::new();
        File::open(path.join("status"))?.read_to_string(&mut s)?;
        Ok(if s.trim() == "Charging" {
            Status::Charging
        } else {
            Status::Discharging
        })
    }

    fn get_charge(prefix: &str, path: &Path) -> io::Result<u64> {
        let mut s = String::new();
        File::open(path.join(String::from(prefix) + "_now"))?.read_to_string(&mut s)?;
        Ok(u64::from_str(s.trim()).unwrap_or(0))
    }

    fn get_info_(prefix: &str, path: &Path) -> io::Result<BatteryInfo> {
        let status = Battery::get_status(path)?;
        let charge = Battery::get_charge(prefix, path)?;
        Ok(BatteryInfo { status, charge })
    }

    fn get_info(&self) -> Option<BatteryInfo> {
        Self::get_info_(self.prefix, &self.path).ok()
    }

    fn symbol(icons: &dyn Icons, status: Status, value: u8) -> String {
        if status == Status::Charging {
            return icons.plug();
        }
        let level = Item::percent_level(value);
        let colour = Item::level_colour(level);
        colour.apply_to(icons.battery(level))
    }
}
