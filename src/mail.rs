use std::fs;
use std::io;

use display_item::{Action, Colour, DisplayItem, Icons, Item, Notification};

pub struct Mail {
    inbox: String,
    has_email: bool,
    old_has_email: bool,
}

impl DisplayItem for Mail {
    fn display(&self, icons: &dyn Icons) -> Item {
        if self.has_email {
            let fmt = Colour::Red.apply_to(icons.mail()).to_string();
            Item::from_markup(fmt.as_str())
        } else {
            Item::empty()
        }
    }

    fn refresh(&mut self, i: u32) -> Vec<Box<dyn Action>> {
        if i % 10 == 0 {
            self.old_has_email = self.has_email;
            self.has_email = self.check().unwrap_or(false);
            if self.has_email && !self.old_has_email {
                return vec![Box::new(Notification::new(
                    "New mail".to_string(),
                    "You have unread emails".to_string(),
                    Some("mail-unread".to_string()),
                    Some("message".to_string()),
                ))];
            }
        }

        Vec::new()
    }
}

impl Mail {
    pub fn new(inbox: String) -> Mail {
        Mail {
            inbox,
            has_email: false,
            old_has_email: false,
        }
    }

    fn check(&self) -> io::Result<bool> {
        let mut entries = fs::read_dir(self.inbox.clone() + "/new")?;
        Ok(entries.next().is_some())
    }
}
