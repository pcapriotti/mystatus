use chrono;
use display_item::{Colour, DisplayItem, Icons, Item};

pub struct Clock {}

impl DisplayItem for Clock {
    fn display(&self, icons: &dyn Icons) -> Item {
        let now = chrono::Local::now();
        let fmt = format!(
            "{} %Y-%m-%d {} %H:%M:%S",
            Colour::Teal.apply_to(icons.calendar()),
            Colour::Teal.apply_to(icons.clock())
        );
        Item::from_markup(now.format(fmt.as_str()).to_string().as_str())
    }
}
