use std::process::Command;
use std::str;
use std::str::FromStr;

use display_item::{Action, DisplayItem, Icons, Item};

#[derive(Clone, Copy)]
pub struct Disk {
    available: u64,
    total: u64,
}

impl DisplayItem for Disk {
    fn refresh(&mut self, i: u32) -> Vec<Box<dyn Action>> {
        if i % 10 == 0 {
            *self = Disk::new()
        }
        Vec::new()
    }

    fn display(&self, icons: &dyn Icons) -> Item {
        let fmt = format!(
            "{} {} {}",
            icons.disk(),
            Item::size(self.available * 1000),
            Item::percent(self.percent())
        );
        Item::from_markup(fmt.as_str())
    }
}

impl Disk {
    pub fn new() -> Disk {
        let home = std::env::var("HOME").unwrap_or("/".to_string());
        let cmd = match Command::new("df").arg(home).output() {
            Err(_) => {
                return Disk {
                    total: 1,
                    available: 0,
                }
            }
            Ok(c) => c,
        };
        let out = str::from_utf8(&cmd.stdout).unwrap_or("");
        let line = out.split('\n').nth(1).unwrap_or("");
        let values: Vec<_> = line.split_whitespace().collect();
        let total = u64::from_str(values[1]).unwrap_or(1);
        let available = u64::from_str(values[3]).unwrap_or(0);
        Disk { total, available }
    }

    fn percent(&self) -> u8 {
        (self.available * 100 / self.total) as u8
    }
}
